# README

## Installing Tidal Cycles

- Install Supercollider (https://supercollider.github.io/download)
- Install haskell (`brew cask install haskell-platform`)
- Run:

```
  cabal install tidal
  <!-- stack setup -->
  <!-- stack install tidal -->
```

- Open Supercollider
- `include("SuperDirt")` then Shift + Enter

## Starting Tidal Cycles

- Open Supercollider
- Run: `SuperDirt.start`